# rest-api-response-format
REST API response format based on some of the best practices

## Rest API Popular Endpoint Formats

> https://api.example.com/v1/items

## Rest API Success Responses

1- GET - Get single item - HTTP Response Code: **200**
``` 
    Content-Type: application/json

    {
        "data": {
            "id": 2,
            "name": "fuchsia rose",
            "year": 2001,
            "color": "#C74375",
            "pantone_value": "17-2031"
        }
    
    }
```
2- GET - Get item list paginate - HTTP Response Code: **200**
```
    HTTP/1.1  201
    Location: /v1/items?page=1&per_page=6
    Content-Type: application/json
    
    {
        "page": 2,
        "per_page": 6,
        "total": 12,
        "total_pages": 2,
        "data": [
            {
                "id": 7,
                "email": "michael.lawson@reqres.in",
                "first_name": "Michael",
                "last_name": "Lawson",
                "avatar": "https://api.examcple.com/img/faces/7-image.jpg"
            }
        ]
    }
```

3- POST - Create a new item - HTTP Response Code: **201**
```
    HTTP/1.1  201
    Location: /v1/items
    Content-Type: application/json
 
    {
        "message": "The item was created successfully",
        "data": {
                "id": 7,
                "email": "michael.lawson@reqres.in",
                "first_name": "Michael",
                "last_name": "Lawson",
                "avatar": "https://api.examcple.com/img/faces/7-image.jpg"
        }
    }
```
4- PATCH - Update an item - HTTP Response Code: **200/204** 

> If updated entity is to be sent after the update

```
    HTTP/1.1  200
    Content-Type: application/json
 
    {
        "message": "The item was updated successfully",
        "data": {
                "id": 7,
                "email": "michael.lawson@reqres.in",
                "first_name": "Michael",
                "last_name": "Lawson",
                "avatar": "https://api.examcple.com/img/faces/7-image.jpg"
        }
    }
```

> If updated entity is not to be sent after the update

```
    HTTP/1.1  204
    {
        "message": "The item was updated successfully"
    }
```

5- DELETE - Delete an item - HTTP Response Code: **204**
```
    HTTP/1.1  204
    {
        "message: "The item is deleted successfully"
    }
```


## Rest API Error Responses

1- GET - HTTP Response Code: **404**

```
    HTTP/1.1  404
    Content-Type: application/json
 
    {
      "message": "The item does not exist"
    }
```
2- DELETE - HTTP Response Code: **404**
```
    HTTP/1.1  404
    Content-Type: application/json
 
    {
      "message": "The item does not exist"
    }
```
3- POST -  HTTP Response Code: **400**
```
    HTTP/1.1  400
    Content-Type: application/json
    
      
    {
        "message": "Validation errors in your request", 
        "errors": {
            "email": [
                  "Oops! The email is invalid"
                ],
            "phoneNumber": [
                  "Oops! The phone number format is not correct"
                ]
        }
    }

```
4- PATCH -  HTTP Response Code: **400/404**
```
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "message": "Validation errors in your request", 
        "errors": [
            {
                "message": "Oops! The format is not correct",
                "code": 35,
                "field": "phoneNumber"
            }
        ]
    }
    
    
    HTTP/1.1  404
    Content-Type: application/json
 
    {
      "message": "The item does not exist"
    }
```
5- VERB Unauthorized - HTTP Response Code: **401**
```
    HTTP/1.1  401
    Content-Type: application/json
 
    {
      "message": "Authentication credentials were missing or incorrect"
    }
```
6- VERB Forbidden - HTTP Response Code: **403**
```
    HTTP/1.1  403
    Content-Type: application/json
 
    {
      "message": "The request is understood, but it has been refused or access is not allowed"
    }
```
7- VERB Conflict - HTTP Response Code: **409**
```
     HTTP/1.1  409
    Content-Type: application/json
 
    {
      "message": "Any message which should help the user to resolve the conflict"
    }
```
8- VERB Too Many Requests - HTTP Response Code: **429**
```
    HTTP/1.1  429
    Content-Type: application/json
 
    {
      "message": "The request cannot be served due to the rate limit having been exhausted for the resource"
    }
```
9- VERB Internal Server Error - HTTP Response Code: **500**
```
    HTTP/1.1  500
    Content-Type: application/json
 
    {
      "message": "Something is broken"
    }
```
10- VERB Service Unavailable - HTTP Response Code: **503**
```
    HTTP/1.1  503
    Content-Type: application/json
 
    {
      "message": "The server is up, but overloaded with requests. Try again later!"
    }
```
## Validation Error Formats

Validation error formats can be different depending on your requirements. Following are some other popular formats, other than the one used above.

```
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "message": "Validation errors in your request"
        "errors": [
            {
                "message": "Oops! The value is invalid",
                "code": 34,
                "field": "email"
            },
            {
                "message": "Oops! The format is not correct",
                "code": 35,
                "field": "phoneNumber"
            }
        ]
    }
```
```
    HTTP/1.1  400
    Content-Type: application/json
    
    {
        "message": "Validation errors in your request", /* skip or optional error message */
        "errors": {
            "email": [
              {
                "message": "Oops! The email is invalid",
                "code": 35
              }
            ],
            "phoneNumber": [
              {
                "message": "Oops! The phone number format is not correct",
                "code": 36
              }
            ]
        }
    }
```
